all:
	pdflatex dporengine.tex
	bibtex dporengine
	pdflatex dporengine.tex
	pdflatex dporengine.tex

clean:
	rm -f *.pdf *.aux *.log *.out *.bbl *.blg *.fls *.fdb_latexmk *.lof *.lol *.lot *.toc
